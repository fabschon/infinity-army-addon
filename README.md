### Infinity Army Addon

___


***To add new troops/skills to the database:***

* Enter missing skills into `infinity_troop_skills.txt`.
* Enter missing troops into `infinity_troops.txt`.

***To add troop entries to a .pdf:***

* Change parameters at the top of `army_annotater_troops.tex` as needed:

![](https://i.imgur.com/d9SIkys.png)

* Add troops like so:

![](https://i.imgur.com/8rhHkg1.png)

You should get something like this:

![](https://i.imgur.com/tGoik6c.png)

It's also possible to add only the [skill entries](https://i.imgur.com/jsXvASV.png) using `army_annotater_list.tex`.
