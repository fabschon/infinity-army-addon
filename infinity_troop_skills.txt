% -- A
\newcommand{\AdvancedECM}       {\troopentry{Advanced ECM}{Against Guided cancels +6 MOD and imposes -3 MOD.}}
\newcommand{\AirborneInfiltration}{\troopentry{AD: Airborne Infiltration}{Use own Order to deploy from edge (Long Skill).}}
\newcommand{\AutoMediKit}       {\troopentry{AutoMediKit}{Recover from Unconscious on PH Roll (Long Skill).}}

% -- B
\newcommand{\BasicImpersonation}{\troopentry{Basic Impersonation}{Deploy freely; hidden twice. Surprise Att. \& Sh. L1.}}
\newcommand{\Bioimmunity}       {\troopentry{Bioimmunity}{Ignores Bio-Munitions such as Shock and Viral.}}

% -- C
\newcommand{\Camouflage}        {\troopentry{CH: Camouflage}{Camo Deployment; -3 MOD to enemy BS attacks.}}
\newcommand{\ClimbingPlus}      {\troopentry{Climbing Plus}{Climb becomes Short Skill; treats vert. surfaces as horiz.}}
\newcommand{\CombatJump}        {\troopentry{AD: Combat Jump}{Deploy anywhere on PH Roll, incl.\ Enemy Deployment Zone.}}
\newcommand{\Courage}            {\troopentry{V1: Courage}{Choose outcome of Guts Rolls; ignore Retreat!}}

% -- D
\newcommand{\DCharges}          {\troopentry{D-Charges}{Plant on terrain or immobilized Troops; detonate w/ 2nd Order.}}
\newcommand{\Deactivator}       {\troopentry{Deactivator}{Pass WIP roll w/ ONLY range MODs to remove deployed weapons.}}
\newcommand{\DeployableRepeater}{\troopentry{Depl. Repeater}{Extends friendly Hacking Areas by own Zone of Control.}}
\newcommand{\DoctorPlus}        {\troopentry{Doctor Plus}{Heals on WIP+3.}}
\newcommand{\Dogged}            {\troopentry{V2: Dogged}{Stay alive until end of Turn when shot Unconscious.}}

% -- F
\newcommand{\FatalityI}         {\troopentry{Fatality L1}{+1 MOD to the Damage of the Troop's BS weapon.}}
\newcommand{\FatalityII}        {\troopentry{Fatality L2}{Rolling a 1 during a BS Attack counts as a Critical.}}
\newcommand{\ForwardObserver}   {\troopentry{Forward Observer}{Can push buttons. Flash Pulse.}}
\newcommand{\FreeAgent}         {\troopentry{Free Agent}{At start of turn Troop may switch Combat Groups.}}

% -- H
\newcommand{\HoloprojectorII}   {\troopentry{Holoprojector L2}{Deploy w/ Holoechos; re-use for free; Surprise Shot L1.}}

% -- I
\newcommand{\Infiltration}      {\troopentry{Infiltration}{Deploy up to the center line or beyond (PH-3 Roll).}}

% -- K
\newcommand{\KinematikaI}       {\troopentry{Kinematika L1}{Engange and Dodge range +1 inch during AROs.}}

% -- M
\newcommand{\MarksmanshipII}    {\troopentry{Marksmanship L2}{Gains Shock ammo; ignores enemy Cover MOD.}}
\newcommand{\MartialArtsIII}    {\troopentry{Martial Arts L3}{+3 CC to self and -3 CC to opp. during CC Attacks.}}
\newcommand{\MediKit}           {\troopentry{MediKit}{Heal both from afar and in base contact on a PH-3.}}
\newcommand{\Medjector}         {\troopentry{Medjector}{Friendly Troop heals on successful PH roll.}}
\newcommand{\Mimetism}          {\troopentry{Mimetism}{-3 MOD to enemy BS Attacks.}}
\newcommand{\Minelayer}         {\troopentry{Minelayer}{Lay one Mine during the Deployment Phase.}}
\newcommand{\MSVII}             {\troopentry{Multispectral Visor L2}{Ignore CH MODs and Low/Zero Visibility Zones.}}
\newcommand{\Multiterrain}      {\troopentry{Multiterrain}{Ignore neg. effects when moving through chosen diff. terrain.}}

% -- P
\newcommand{\Parachutist}       {\troopentry{AD: Parachutist}{Use own Order to deploy from pre-chose edge (Long Skill).}}
\newcommand{\Poison}            {\troopentry{Poison}{Adds BTS Roll to successful CC attacks (Wounds only).}}

% -- R
\newcommand{\Regeneration}      {\troopentry{Regeneration}{Pass PH roll to regenerate one Wound.}}
\newcommand{\Religious}         {\troopentry{Religious Troop}{Must pass Guts Roll to seek cover when shot.}}
\newcommand{\RemotePresence}    {\troopentry{G: Remote Presence}{Choose res. of Guts Rolls \& 2nd lvl of Unconscious.}}
\newcommand{\Repeater}          {\troopentry{Repeater}{Extend Hacking Area of ALL Hackers by own Zone of Control.}}

% -- S
\newcommand{\SatLock}           {\troopentry{Sat Lock}{WIP-6 Comms Attack w/o MODs/LoF for Targeted and Discover.}}
\newcommand{\Sensor}            {\troopentry{Sensor}{WIP+6 w/o MODs/LoF to Discover ALL enemies within Sensor Area.}}
\newcommand{\Servant}           {\troopentry{G: Servant}{Activate w/ attached Troop, allow for remote actions.}}
\newcommand{\SixthSenseI}       {\troopentry{Sixth Sense L1}{Ignores neg. MODs when attacked within his Zone of Ctrl.}}
\newcommand{\Sniffer}           {\troopentry{Sniffer}{Extend friendly Sensor Areas by own Zone of Control.}}
\newcommand{\SpecialistOperative}{\troopentry{Specialist Operative}{Troop counts as a Specialist.}}
\newcommand{\SuperJump}         {\troopentry{Super-Jump}{Jumping becomes Short Skill; may also Long Skill jump.}}
\newcommand{\SurpriseAttack}    {\troopentry{Surprise Attack}{Op. suffers -6 MOD when CC-attacked out of Camo.}}
\newcommand{\SurpriseShotI}     {\troopentry{Surprise Shot L1}{Op. suffers -3 MOD when attacked out of Camo.}}
\newcommand{\SurpriseShotII}    {\troopentry{Surprise Shot L2}{Op. suffers -6 MOD when attacked out of Camo.}}

% -- T
\newcommand{\TOCamo}            {\troopentry{CH: TO Camo}{Hidden Deployment; -6 MOD to enemy BS attacks.}}
\newcommand{\TotalReaction}     {\troopentry{Total Reaction}{Use full Burst value during AROs.}}

% -- V
\newcommand{\VeteranI}          {\troopentry{Veteran L1}{Ignores Loss of Lieutenant, Retreat!, and Isolated.}}

% -- X
\newcommand{\XVisor}            {\troopentry{X-Visor}{-3/-6 Range MODS become 0/-3.}}
